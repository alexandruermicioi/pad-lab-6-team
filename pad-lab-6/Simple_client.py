from  collections import OrderedDict
from itertools import  groupby
from collections import Counter
import requests
from  operator import  itemgetter
from lxml import etree
from StringIO import *
from xml.dom.minidom import parseString
import xmltodict
from dicttoxml import dicttoxml

proxy_add = 'http://127.0.0.4:8083'
xml_schema_add = 'Schema.xsd'


ronnie = '''<?xml version="1.0" encoding="UTF-8" ?>
<root>
        <item>
                <Department>HR</Department>
                <Salary>8500</Salary>
                <Surname>O'Sullivan</Surname>
                <Name>Ronnie</Name>
        </item>
</root>'''

marko = '''<?xml version="1.0" encoding="UTF-8" ?>
<root>
        <item>
                <Department>Production</Department>
                <Salary>3500</Salary>
                <Surname>Fu</Surname>
                <Name>Marko</Name>
        </item>
</root>'''

john = '''<?xml version="1.0" encoding="UTF-8" ?>
<root>
        <item>
                <Department>Production</Department>
                <Salary>5500</Salary>
                <Surname>Higgins</Surname>
                <Name>John</Name>
        </item>
</root>'''

smith = '''<?xml version="1.0" encoding="UTF-8" ?>
<root>
        <item>
                <Department>Aperture</Department>
                <Salary>3000</Salary>
                <Surname>Smith</Surname>
                <Name>John</Name>
</item>
</root>'''

xml = '''<?xml version="1.0" encoding="UTF-8" ?>
<root>
        <item>
                <Department>Test_dep</Department>
                <Salary>500</Salary>
                <Surname>Test_sur</Surname>
                <Name>Test_name</Name>
                <_id>10001</_id>
</item>
</root>'''

xml2 = '''<?xml version="1.0" encoding="UTF-8" ?>
<root>
        <item>
                <Department>T_dep</Department>
                <Salary>9999</Salary>
                <Surname>T_sur</Surname>
                <Name>T_name</Name>
                <_id>10001</_id>
</item>
</root>'''



def validate(xml_doc, xml_schema):
    f = StringIO(xml_schema)
    xmlschema_doc = etree.parse(f)
    xmlschema = etree.XMLSchema(xmlschema_doc)

    valid = StringIO(xml_doc)
    doc = etree.parse(valid)
    return xmlschema.validate(doc)

def get(add):
    print 'GET ', add
    r = requests.get(add)
    dom = parseString(r.text)
    xml_doc = dom.toprettyxml()

    return xml_doc

def get_all():
    add = proxy_add + '/worker/'
    xml_doc = get(add)

    print xml_doc

    if validate(xml_doc, xml_schema):
        print "XML is valid"
    else:
        print "XML is invalid"

    return xml_doc

def get_one(id):
    add = proxy_add + '/worker/' + str(id) + '/'
    xml_doc = get(add)

    print xml_doc

    if validate(xml_doc, xml_schema):
        print "XML is valid"
    else:
        print "XML is invalid"

def put_one(da):
    add = proxy_add + '/worker/'
    print 'PUT ', add
    r = requests.put( add, data = da )
    print 'id = ', r.text

def delete_one(id):
    add = proxy_add + '/worker/' + str(id) + '/'
    print 'DELETE ', add
    r = requests.delete(add)

def delete_all():
    add = proxy_add + '/worker/'
    print 'DELETE ', add
    r = requests.delete(add)

def update_one(id, da):
    add = proxy_add + '/worker/' + str(id) + '/'
    print 'POST ', add
    r = requests.post( add, data = da )



def test_xml(add = proxy_add):
##    add = 'http://' + add

    print "Good"
    r = requests.get(add + '/worker/')
    print r
    print r.text
    print

    r = requests.put(add + '/worker/', data = xml)
    print r
    print r.text
    print

    r = requests.get(add + '/worker/10001/')
    print r
    print r.text
    print

    r = requests.post(add + '/worker/10001/', data = xml2)
    print r
    print r.text
    print

    print "Bad"
    r = requests.get(add + '/worker/1/')
    print r
    print r.text
    print

    print "Wrong"
    r = requests.get(add + '/worer/')
    print r
    print r.text
    print
    r = requests.get(add + '/worker')
    print r
    print r.text
    print

    r = requests.get(add + '/worker/10001')
    print r
    print r.text
    print
    r = requests.get(add + '/worer/10001/')
    print r
    print r.text
    print

    r = requests.put(add + '/worer/', data = xml)
    print r
    print r.text
    print
    r = requests.put(add + '/worker', data = xml)
    print r
    print r.text
    print



def sort_by(f, a_xml):
    print 'Sorted'
    a_dict = xmltodict.parse(a_xml)

    ans_dict = sorted(a_dict['root']['item'], key=f)

    ans_xml = dicttoxml(ans_dict, attr_type=False)
    dom = parseString(ans_xml)
    print dom.toprettyxml()

def filter_by(f, a_xml):
    print 'Filtered'
    a_dict = xmltodict.parse(a_xml)

    ans_dict = list(filter(f, a_dict['root']['item']))

    ans_xml = dicttoxml(ans_dict, attr_type=False)
    dom = parseString(ans_xml)
    print dom.toprettyxml()

def group_by(f, a_xml):
    print 'Grouped'
    groups = []
    uniquekeys = []
    
    a_dict = xmltodict.parse(a_xml)
    ans_dict = sorted(a_dict['root']['item'], key=f)
    
    for k, g in groupby(ans_dict, f):
        groups.append(list(g))      # Store group iterator as a list
        uniquekeys.append(k)

    repetitions = [len(x) for x in groups]
    print [(x,y) for x, y in zip(uniquekeys, repetitions)]



if __name__ == '__main__':

    with open(xml_schema_add, 'r') as f:
        xml_schema = f.read()
##
##    delete_all()
##
##    put_one(ronnie)
##    put_one(marko)
##    put_one(john)
##    put_one(smith)
##    put_one(xml)
##    
##    get_all()
##    get_one(10001)
##
##    update_one(10001, xml2)
##    get_one(10001)
##    
##    
##    get_all()
##    get_one(10001)
##    delete_one(10001)
##    get_all()
##    
##
##    filter_by(lambda x: x['Name'] == 'John', get_all())
##    sort_by(lambda x: x['Salary'], get_all())
##    group_by(lambda x: x['Department'], get_all())
